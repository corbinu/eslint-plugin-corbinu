import BaseRc from "./baserc.json";
import BrowserRc from "./browserrc.json";
import JestRc from "./jestrc.json";
import NodeRc from "./noderc.json";

export = {
    configs: {
        base: BaseRc,
        jest: JestRc,
        node: NodeRc,
        browser: BrowserRc,
    },
};
